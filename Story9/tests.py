from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
import time

from Story9.views import *

class Story9UnitTest(TestCase):
    def test_story9_url_is_exist(self):
        # Check if login page returns 200
        response = Client().get('/acc/')
        self.assertEqual(response.status_code,200)

    def test_story9_using_login_func(self):
    	# Check if views is using 'logIn' function
    	found = resolve('/acc/')
    	self.assertEqual(found.func, logIn)
    
    def test_story9_using_login_template(self):
        # Check if login page is using 'login.html' template
        response = Client().get('/acc/')
        self.assertTemplateUsed(response, 'login.html')

    def test_landing_page_after_login(self):
        # Check if landing page opens after logging in
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/landing/')
        self.assertEqual(response.status_code, 200)
    
    def test_logout_redirect(self):
        # Check if page is redirected after logout
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        self.client.login(username='john', password='johnpassword')
        response = self.client.post('/acc/logout')
        self.assertEqual(response.status_code, 302)


class Story9FunctionalTest(LiveServerTestCase):
    def setUp(self):
        # setup selenium browser
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story9FunctionalTest, self).setUp()

    def tearDown(self):
        # set tearing down
        self.browser.quit()
        super(Story9FunctionalTest, self).tearDown()

    def test_login(self):
        # try inputing in login page
        self.browser.get(self.live_server_url)
        time.sleep(2)
        new_username = self.browser.find_element_by_id('username')
        new_password = self.browser.find_element_by_id('password')
        submit = self.browser.find_element_by_id('submit')
        new_username.send_keys('alviadibby')
        new_password.send_keys('123')
        time.sleep(2)

