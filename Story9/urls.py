from django.urls import path
from . import views

app_name= 'Story9'
urlpatterns = [
    path('', views.logIn, name='login'),
    path('login', views.logIn, name='login'),
    path('logout', views.logOut, name='logout')

]