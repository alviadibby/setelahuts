from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout

def logIn(request):
    if request.user.is_authenticated:
        return redirect('/landing')   

    else:
        if request.method == 'GET':
            return render(request,'login.html')
        elif request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/acc')
            else:
                return redirect('/acc')

def logOut(request):
    logout(request)
    return redirect('/acc')