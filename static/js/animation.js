/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
document.getElementById("mySidenav").style.width = "0";
document.getElementById("main").style.marginLeft = "0";
}


$(document).ready(function(){
/* Story 7 */
/* Making accordion */
	$(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
          heightStyle: "content",
        });
    });

/* Changing theme */
    $("#theme_switch").click(() => {
        let body = document.querySelector('body')
        if (body.classList.contains('light')) {
            body.classList.remove('light');
            body.classList.add('dark');
            document.getElementById("theme").innerHTML = "Mau terang?";
        }
        else {
            body.classList.remove('dark');
            body.classList.add('light');
            document.getElementById("theme").innerHTML = "Mau gelap?";
        }
    });

/* Story 8 */
    if (window.location.href.match(/books/)) {
        find("ajax");
        $("#book_input").on("keyup", delay(function(e) {
            bookTitle = e.currentTarget.value.toLowerCase()
            find(bookTitle)
        }, 500));
    }

});

var maxPage;
var currentPage;
var title;

/* Delay settings */
function delay(fn, ms) {
    let timer = 0
    return function(...args) {
      clearTimeout(timer)
      timer = setTimeout(fn.bind(this, ...args), ms || 0)
    }
  }

/* Finding book title */
function find(bookTitle){
    $.ajax({
        url: "data/bookTitle=" + bookTitle,
        datatype: 'json',
        success: function(data){
            maxPage = Math.floor(data.totalItems/10);
            currentPage = 0;
            title = bookTitle;
            pageStatus();
            $('tbody').empty();
            var result ='<tr>';

            if (data.totalItems == 0) {
                alert("Buku tidak ditemukan");
                window.location = '/books/';
            } else {
            for(var i = 0; i < data.items.length; i++) {
                try{
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                    "<td><img class='img-fluid' style='width:22vh' src='" 
                    + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + "</td></tr>";
                }
                catch{

                }
            }
            $('tbody').append(result);
            }
        },

    });

}

/* For search button */
function search() {
    var word = $("#book_input").val();
    find(word);
}

/* If next */
$('#next').click(() => {
    if(currentPage<maxPage){
        currentPage+=1;
        $.ajax({
            url: "data/bookTitle="+title+"&startIndex="+10*currentPage,
            success: function(data){
            pageStatus();
            $('tbody').empty();
            var result ='<tr>';
            for(var i = 0; i < data.items.length; i++) {
                try{
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (10*currentPage+i+1) + "</th>" +
                    "<td><img class='img-fluid' style='width:22vh' src='" 
                    + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + "</td></tr>";
                }
                catch{

                }
            }
            $('tbody').append(result);
            },
        });
    }
});

/* If prev */
$('#prev').click(() => {
    if(currentPage>0){
        currentPage-=1;
        $.ajax({
            url: "data/bookTitle="+title+"&startIndex="+10*currentPage,
            success: function(data){
            pageStatus();
            $('tbody').empty();
            var result ='<tr>';
            for(var i = 0; i < data.items.length; i++) {
                try{
                    result += "<tr> <th scope='row' class='align-middle text-center'>" + (10*currentPage+i+1) + "</th>" +
                    "<td><img class='img-fluid' style='width:22vh' src='" 
                    + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                    "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                    "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + "</td></tr>";
                }
                catch{
                }
            }
            $('tbody').append(result);
            },
        });
    }
});

/* Able or disabling page button */
function pageStatus(){
    if(currentPage>0){
        $('#prev')[0].className = "page-item"
    } else {
        $('#prev')[0].className = "page-item disabled"
    }
    if(currentPage<maxPage){
        $('#next')[0].className = "page-item"
    } else {
        $('#next')[0].className = "page-item disabled"
    }
    $('#current')[0].innerHTML = currentPage+1;
}

