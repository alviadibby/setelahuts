from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from Story7.views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class Story7UnitTest(TestCase):
    def test_story7_url_is_exist(self):
        # Check if profile page returns 200
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)
    
    def test_story7_using_profile_func(self):
    	# Check if views is using 'profile' function
    	found = resolve('/profile/')
    	self.assertEqual(found.func, profile)
    
    def test_story7_using_profile_template(self):
        # Check if landing page is using 'profile.html' template
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_story7_contains_greeting(self):
        # Check if greeting is in page
        response = Client().get('/profile/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Profileku", response_content)

class Story7FunctionalTest(LiveServerTestCase):

    def setUp(self):
        # setup selenium browser
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        # set tearing down
        self.browser.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_js_accordian_one(self):
        # Check if accordion 1 could be opened & have content
        browser = self.browser
        browser.get(self.live_server_url + '/profile/')
        time.sleep(2)
        self.browser.find_element_by_id('ui-id-1').click()
        self.assertIn("tidur", browser.page_source)
        time.sleep(2)
        
        # Check if accordion 2 could be opened & have content
        browser = self.browser
        browser.get(self.live_server_url + '/profile/')
        time.sleep(2)
        self.browser.find_element_by_id('ui-id-3').click()
        self.assertIn("Staff Humas BEM Fasilkom UI", browser.page_source) 
        time.sleep(2)

        # Check if accordion 3 could be opened & have content
        browser = self.browser
        browser.get(self.live_server_url + '/profile/')
        time.sleep(2)
        self.browser.find_element_by_id('ui-id-5').click()
        self.assertIn("teu aya hiks", browser.page_source) 
        time.sleep(2)

    def test_js_theme_change(self):    
        # Check if background color could change
        browser = self.browser
        browser.get(self.live_server_url + '/profile/')
        time.sleep(2)
        
        background_color = browser.find_element_by_id('body').value_of_css_property('background-color')
        self.assertIn("rgba(255, 255, 255, 1)", background_color)
        time.sleep(2)

        browser.find_element_by_class_name('switch').click()
        background_color = browser.find_element_by_id('body').value_of_css_property('background-color')
        self.assertIn("rgba(20, 20, 20, 1)", background_color)
        time.sleep(2)

        # Check if font color could change
        browser = self.browser
        browser.get(self.live_server_url + '/profile/')
        time.sleep(2)
        
        font_color = browser.find_element_by_id('body').value_of_css_property('color')
        self.assertIn("rgba(20, 20, 20, 1)", font_color)
        time.sleep(2)

        browser.find_element_by_class_name('switch').click()
        time.sleep(2)
        font_color = browser.find_element_by_id('body').value_of_css_property('color')
        self.assertIn("rgba(255, 255, 255, 1)", font_color)
        time.sleep(2)

