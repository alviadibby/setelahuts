/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
document.getElementById("mySidenav").style.width = "0";
document.getElementById("main").style.marginLeft = "0";
}


$(document).ready(function(){
/* Story 7 */
/* Making accordion */
	$(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
          heightStyle: "content",
        });
    });

/* Changing theme */
    $("#theme_switch").click(() => {
        let body = document.querySelector('body')
        if (body.classList.contains('light')) {
            body.classList.remove('light');
            body.classList.add('dark');
            document.getElementById("theme").innerHTML = "Mau terang?";
        }
        else {
            body.classList.remove('dark');
            body.classList.add('light');
            document.getElementById("theme").innerHTML = "Mau gelap?";
        }
    });

/* Story 8 */
    find("ajax");
    $("#book_input").on("keyup", function(e) {
        bookTitle = e.currentTarget.value.toLowerCase()
        find(bookTitle)
    });

});


function find(bookTitle){
    $.ajax({
        url: "data/bookTitle=" + bookTitle,
        datatype: 'json',
        success: function(data){
            $('tbody').empty();
            var result ='<tr>';

            if (data.items.length != 10) {
                alert("Buku tidak ditemukan");
                $("#book_input").val("");
            } else {
            for(var i = 0; i < data.items.length; i++) {
                result += "<tr> <th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" 
                + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + "</td></tr>";
            }
            $('tbody').append(result);
            }
        },

    })

}


function search() {
    var word = $("#book_input").val();
    find(word);
}
