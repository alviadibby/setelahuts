$(document).ready(function(){

	$(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
        });
    });

    $("input").click(() => {
        let body = document.querySelector('body')
        if (body.classList.contains('morning')) {
            body.classList.remove('morning');
            body.classList.add('sunset');
            document.getElementById("greeting").innerHTML = "enjoy the sunset:)";
        }
        else {
            body.classList.remove('sunset');
            body.classList.add('morning');
            document.getElementById("greeting").innerHTML = "good morning!";
        }
    
    });

});

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}
