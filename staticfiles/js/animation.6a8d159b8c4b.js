/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  
  /* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
document.getElementById("mySidenav").style.width = "0";
document.getElementById("main").style.marginLeft = "0";
}

$(document).ready(function(){

	$(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
          heightStyle: "content",
        });
    });

    $("input").click(() => {
        let body = document.querySelector('body')
        if (body.classList.contains('light')) {
            body.classList.remove('light');
            body.classList.add('dark');
            document.getElementById("theme").innerHTML = "Mau terang?";
        }
        else {
            body.classList.remove('dark');
            body.classList.add('light');
            document.getElementById("theme").innerHTML = "Mau gelap?";
        }
    });
    

});

