from django.urls import path
from . import views

app_name= 'Story6'
urlpatterns = [
    path('', views.landing, name='landing')
]