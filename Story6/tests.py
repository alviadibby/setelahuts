from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from Story6.views import *
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class Story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        # Check if landing page returns 200
        response = Client().get('/landing/')
        self.assertEqual(response.status_code,200)
    
    def test_story6_using_landing_func(self):
    	# Check if views is using 'landing' function
    	found = resolve('/landing/')
    	self.assertEqual(found.func, landing)
    
    def test_story6_using_landing_template(self):
        # Check if landing page is using 'landing.html' template
        response = Client().get('/landing/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_story6_contains_greeting(self):
        # Check if greeting is in page
        response = Client().get('/landing/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Selamat datang", response_content)

    def test_status_request_post_success(self):
        # Check if status can be posted and rendered
        new_status = 'Coba Coba'
        response_post = Client().post('/landing/', {'status': new_status})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/landing/')
        html_response = response.content.decode('utf8')
        self.assertIn(new_status, html_response)

    def test_model_can_create_new_activity(self):
        # Check if status is created
    	new_status = Status.objects.create(status = "Coba Coba")
    	counting_all_available_status = Status.objects.all().count()
    	self.assertEqual(counting_all_available_status, 1)

class Story6FunctionalTest(LiveServerTestCase):

    def setUp(self):
        # setup selenium browser
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        # set tearing down
        self.browser.quit()
        super(Story6FunctionalTest, self).tearDown()

    def test_post(self):
        # test open & posting on web
        self.browser.get(self.live_server_url+'/landing')
        time.sleep(2)
        self.browser.find_element_by_id('id_status').send_keys("Coba Coba")
        time.sleep(2)
        submit = self.browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.browser.get(self.live_server_url+'/landing/')
        time.sleep(2)
        self.assertIn('Coba Coba', self.browser.page_source)



