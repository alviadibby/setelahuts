from django.shortcuts import render, redirect
from .models import Status
from . import forms


def landing(request):
    status = Status.objects.all().order_by('created_at').reverse()
    if request.method == "POST":
        form = forms.StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('Story6:landing')
    else:
        form = forms.StatusForm()
    return render (request, 'landing.html', {'form' : form, 'status' : status})