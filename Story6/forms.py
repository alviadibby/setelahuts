from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    error_msg = {
    'required' : 'This field is required',
    }

    status = forms.CharField(
        label = '',
        widget = 
        forms.Textarea(attrs={
        'max_length' : 300,
        'cols' : 10,
        'rows' : 5,
        'class' : 'form-control',
        'placeholder' : 'Apa yang sedang kamu pikirkan?'
        }))

    class Meta:
        model = Status
        fields = ['status']