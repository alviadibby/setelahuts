from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from Story8.views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class Story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        # Check if books page returns 200
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)
    
    def test_story8_json_data_url_exists(self):
        # Check if json data page returns 200
        response = Client().get('/books/data/bookTitle=<str:bookTitle>')
        self.assertEqual(response.status_code, 200)

    def test_story8_using_books_func(self):
    	# Check if views is using 'books' function
    	found = resolve('/books/')
    	self.assertEqual(found.func, books)
    
    def test_story8_using_data_func(self):
        # Check if views is using 'data' function
        found = resolve('/books/data/bookTitle=<str:bookTitle>')
        self.assertEqual(found.func, data)
    
    def test_story8_using_books_template(self):
        # Check if books page is using 'books.html' template
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_story8_contains_greeting(self):
        # Check if greeting is in page
        response = Client().get('/books/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Cari buku favoritmu di sini", response_content)

class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self):
        # setup selenium browser
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        # set tearing down
        self.browser.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_ajax(self):
        # test open web & ajax works
        browser = self.browser
        browser.get(self.live_server_url + '/books/')
        time.sleep(2)
        browser.find_element_by_id('book_input').send_keys("Halo")
        time.sleep(5)
