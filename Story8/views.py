from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def books(request):
    return render(request, 'books.html')

def data(request, bookTitle="ajax"):
    link = "https://www.googleapis.com/books/v1/volumes?q=" + bookTitle
    print(link)
    final = requests.get(link).json()
    return JsonResponse(final)
