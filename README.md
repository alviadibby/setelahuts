# Author
Alvia Dibby Shadqah - 1806191004

# Pipeline and Coverage
[![pipeline status](https://gitlab.com/alviadibby/setelahuts/badges/master/pipeline.svg)](https://gitlab.com/alviadibby/setelahuts/commits/master)

[![coverage report](https://gitlab.com/alviadibby/setelahuts/badges/master/coverage.svg)](https://gitlab.com/alviadibby/setelahuts/commits/master)

# Herokuapp Link
http://dibby-setelah-uts.herokuapp.com/